import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'ng-batch-four';

  name = 'Dzurrahman';
  age = 20;
  status = false;

  showData = true;

  nomor = 1

  person = {
    title: 'Test A',
    name: 'ngroki',
    age: 0,
    status: true,
  };

  personList = [
    {
      title: 'Test A',
      name: 'ngroki',
      age: 20,
      status: true,
    },
    {
      title: 'Test B',
      name: 'ngroki',
      age: 20,
      status: false,
    },
  ];

  datas = [1, 2, 3];

  constructor() {
    this.name = 'Roki';
    this.age = 21;
  }

  onCallBack(ev: any) {
    console.log(ev);
    this.personList.push(ev.data)
  }
}
