import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {

  harga = 0

  testClass = 'alert alert-success'

  constructor(private dataService: DataService) {
    this.harga = this.dataService.baseHarga
  }
  
  hitung() {
    if(this.harga === 1000) {
      this.testClass = 'alert alert-success'
    } else {
      this.testClass = 'alert alert-danger'
    }
  }
}

function Hitung() {

}
