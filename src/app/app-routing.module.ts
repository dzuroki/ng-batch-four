import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalculateGasolineComponent } from './pages/calculate-gasoline/calculate-gasoline.component';
import { LoginComponent } from './pages/login/login.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'playground',
    component: PlaygroundComponent
  },
  {
    path: 'calculate',
    component: CalculateGasolineComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: '',
    redirectTo: 'playground',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
